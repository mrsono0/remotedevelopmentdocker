# FROM debian:buster
FROM buildpack-deps:buster

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

ENV SERVICE_URL=https://marketplace.visualstudio.com/vscode
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 SHELL=/bin/bash
ENV LANGUAGE=${LANG} TZ=Asia/Seoul
ENV PATH=/usr/local/coder:/usr/local/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get install -y wget curl bzip2 libglib2.0-0 libxext6 libsm6 libxrender1 git mercurial subversion \
	bash-completion \
	ca-certificates \
    software-properties-common \
    apt-transport-https \
    tzdata vim nano neovim git-lfs \
    openssl locales dumb-init \
    supervisor \
    cron \
    gpg \
	dirmngr 

 # timezone settings
RUN cp /usr/share/zoneinfo/${TZ} /etc/localtime && echo $TZ > /etc/timezone

# Create a non-root user to use if preferred - see https://aka.ms/vscode-remote/containers/non-root-user.
# https://github.com/cdr/code-server/releases
ENV vscode_version=2.1692-vsc1.39.2
ENV vscode_filename=code-server2.1692-vsc1.39.2-linux-x86_64
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # [Optional] Add sudo support for non-root user
    && apt-get install -y sudo \
    nodejs \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && mkdir -p /usr/local/coder \
    && wget https://github.com/cdr/code-server/releases/download/${vscode_version}/${vscode_filename}.tar.gz \
    && tar -xvf ${vscode_filename}.tar.gz \
    && mv ${vscode_filename} coder \
    && mv coder /usr/local/ \
    && chmod +x /usr/local/coder/code-server \
    && ln -s /usr/local/coder/code-server /usr/local/bin/code-server \
    && rm -f ${vscode_filename}.tar.gz \
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN unset vscode_version vscode_filename
COPY ETC/entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/entrypoint.sh

COPY ETC/vscode-icons-team.vscode-icons-9.6.0.vsix \
    ETC/Atlassian.atlascode-2.2.1.vsix \
    ETC/redhat.vscode-yaml-0.6.1.vsix \
    ETC/mhutchie.git-graph-1.20.0.vsix \
    ETC/formulahendry.code-runner-0.9.15.vsix \
    /home/vscode/
RUN code-server --install-extension /home/vscode/vscode-icons-team.vscode-icons-9.6.0.vsix \
    && code-server --install-extension /home/vscode/Atlassian.atlascode-2.2.1.vsix \
    && code-server --install-extension /home/vscode/redhat.vscode-yaml-0.6.1.vsix \
    && code-server --install-extension /home/vscode/mhutchie.git-graph-1.20.0.vsix \
    && code-server --install-extension /home/vscode/formulahendry.code-runner-0.9.15.vsix
USER vscode
RUN code-server --install-extension /home/vscode/vscode-icons-team.vscode-icons-9.6.0.vsix \
    && code-server --install-extension /home/vscode/Atlassian.atlascode-2.2.1.vsix \
    && code-server --install-extension /home/vscode/redhat.vscode-yaml-0.6.1.vsix \
    && code-server --install-extension /home/vscode/mhutchie.git-graph-1.20.0.vsix \
    && code-server --install-extension /home/vscode/formulahendry.code-runner-0.9.15.vsix
USER root

COPY ETC/ms-python.python-2020.1.57204.vsix \
    ETC/CoenraadS.bracket-pair-colorizer-2-0.0.29.vsix \
    ETC/jithurjacob.nbpreviewer-1.2.2.vsix \
    ETC/donjayamanne.python-extension-pack-1.6.0.vsix \
    ETC/formulahendry.code-runner-0.9.15.vsix \
    ETC/tht13.python-0.2.3.vsix \
    /home/vscode/

RUN code-server --install-extension /home/vscode/ms-python.python-2020.1.57204.vsix && \
    code-server --install-extension /home/vscode/CoenraadS.bracket-pair-colorizer-2-0.0.29.vsix && \
    code-server --install-extension /home/vscode/jithurjacob.nbpreviewer-1.2.2.vsix && \
    code-server --install-extension /home/vscode/donjayamanne.python-extension-pack-1.6.0.vsix && \
    code-server --install-extension /home/vscode/formulahendry.code-runner-0.9.15.vsix && \
    code-server --install-extension /home/vscode/tht13.python-0.2.3.vsix 
USER vscode
RUN code-server --install-extension /home/vscode/ms-python.python-2020.1.57204.vsix && \
    code-server --install-extension /home/vscode/CoenraadS.bracket-pair-colorizer-2-0.0.29.vsix && \
    code-server --install-extension /home/vscode/jithurjacob.nbpreviewer-1.2.2.vsix && \
    code-server --install-extension /home/vscode/donjayamanne.python-extension-pack-1.6.0.vsix && \
    code-server --install-extension /home/vscode/formulahendry.code-runner-0.9.15.vsix && \
    code-server --install-extension /home/vscode/tht13.python-0.2.3.vsix
USER root


COPY ETC/*-java-*.vsix \
  ETC/*.java-*.vsix \
  ETC/vscjava.vscode-maven-0.20.0.vsix \
  ETC/adashen.vscode-tomcat-0.11.1.vsix \
  ETC/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  ETC/*-spring-*.vsix \
  /home/vscode/
RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
    && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
    && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER vscode
RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
    && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
    && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER root

COPY ETC/rust-lang.rust-0.7.0.vsix \
    ETC/ms-python.python-2020.1.57204.vsix \
    /home/vscode/
RUN code-server --install-extension /home/vscode/rust-lang.rust-0.7.0.vsix
USER vscode
RUN code-server --install-extension /home/vscode/rust-lang.rust-0.7.0.vsix
USER root

RUN rm -f /home/vscode/*.vsix

# RUN locale-gen --purge
# RUN locale-gen ko_KR.UTF-8
# RUN dpkg-reconfigure locales
# RUN echo 'LANG="ko_KR.UTF-8"' >> /etc/environment && \
#     # echo 'LANG="ko_KR.EUC-KR"' >> /etc/environment && \
#     echo 'LANGUAGE="ko_KR;ko;en_GB;en"' >> /etc/environment && \
#     echo 'LC_ALL="ko_KR.UTF-8"' >> /etc/environment
# RUN echo 'export LANG="ko_KR.UTF-8"' >> /etc/profile && \
#     # echo 'export LANG="ko_KR.EUC-KR"' >> /etc/profile && \
#     echo 'export LANGUAGE="ko_KR;ko;en_GB;en"' >> /etc/profile && \
#     echo 'export LC_ALL="ko_KR.UTF-8"' >> /etc/profile && \
#     echo "export QT_XKB_CONFIG_ROOT=/usr/share/X11/locale" >> /etc/profile
# RUN echo 'LANG="ko_KR.UTF-8"' >> /etc/default/locale && \
#     # echo 'LANG="ko_KR.EUC-KR"' >> /etc/default/locale && \
#     echo 'LANGUAGE="ko_KR;ko;en_GB;en"' >> /etc/default/locale && \
#     echo 'LC_ALL="ko_KR.UTF-8"' >> /etc/default/locale

# ENV PASSWORD "000000"
EXPOSE 6006-6015 8080 8888 8889
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD [ "/bin/bash" ]