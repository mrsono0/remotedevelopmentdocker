FROM mrsono0/devremotecontainers:vscode-server

# https://github.com/ContinuumIO/docker-images/blob/master/anaconda3/debian/Dockerfile
RUN wget --quiet https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh -O ~/anaconda.sh && \
    /bin/bash ~/anaconda.sh -b -p /opt/conda && \
    rm ~/anaconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    # echo "conda activate base" >> ~/.bashrc && \
    find /opt/conda/ -follow -type f -name '*.a' -delete && \
    find /opt/conda/ -follow -type f -name '*.js.map' -delete && \
    /opt/conda/bin/conda clean -afy

RUN if [ ! -d "/usr/local/anaconda3" ]; then ln -s /opt/conda /usr/local/anaconda3; fi
# RUN pip --disable-pip-version-check --no-cache-dir install pylint
# Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

ENV PATH=/opt/conda/bin:$PATH
RUN bash -c "/opt/conda/bin/conda install jupyter -y --quiet" && \
    bash -c "/opt/conda/bin/conda install jupyterlab -y --quiet" && \
    bash -c "/opt/conda/bin/conda install pylint -y --quiet"

COPY ETC/entrypoint.anaconda3.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

COPY ETC/ms-python.python-2020.1.57204.vsix \
    ETC/CoenraadS.bracket-pair-colorizer-2-0.0.29.vsix \
    ETC/jithurjacob.nbpreviewer-1.2.2.vsix \
    ETC/donjayamanne.python-extension-pack-1.6.0.vsix \
    ETC/formulahendry.code-runner-0.9.15.vsix \
    ETC/tht13.python-0.2.3.vsix \
    /home/vscode/

RUN code-server --install-extension /home/vscode/ms-python.python-2020.1.57204.vsix && \
    code-server --install-extension /home/vscode/CoenraadS.bracket-pair-colorizer-2-0.0.29.vsix && \
    code-server --install-extension /home/vscode/jithurjacob.nbpreviewer-1.2.2.vsix && \
    code-server --install-extension /home/vscode/donjayamanne.python-extension-pack-1.6.0.vsix && \
    code-server --install-extension /home/vscode/formulahendry.code-runner-0.9.15.vsix && \
    code-server --install-extension /home/vscode/tht13.python-0.2.3.vsix
USER vscode
RUN code-server --install-extension /home/vscode/ms-python.python-2020.1.57204.vsix && \
    code-server --install-extension /home/vscode/CoenraadS.bracket-pair-colorizer-2-0.0.29.vsix && \
    code-server --install-extension /home/vscode/jithurjacob.nbpreviewer-1.2.2.vsix && \
    code-server --install-extension /home/vscode/donjayamanne.python-extension-pack-1.6.0.vsix && \
    code-server --install-extension /home/vscode/formulahendry.code-runner-0.9.15.vsix && \
    code-server --install-extension /home/vscode/tht13.python-0.2.3.vsix
USER root
RUN rm -f /home/vscode/*.vsix