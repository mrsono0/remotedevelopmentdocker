FROM mrsono0/devremotecontainers:oraclejdk8

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      net-tools \
      netcat \
      gnupg \
      ssh \
    && rm -rf /var/lib/apt/lists/*

RUN curl -O https://dist.apache.org/repos/dist/release/hadoop/common/KEYS

RUN gpg --import KEYS

ENV HADOOP_VERSION 3.1.3
ENV HADOOP_URL_asc https://www.apache.org/dist/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz
ENV HADOOP_URL http://apache.mirror.cdnetworks.com/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz

RUN set -x \
    && curl -fSL "$HADOOP_URL" -o /tmp/hadoop.tar.gz \
    && curl -fSL "$HADOOP_URL_asc.asc" -o /tmp/hadoop.tar.gz.asc \
    && gpg --verify /tmp/hadoop.tar.gz.asc \
    && tar -xvf /tmp/hadoop.tar.gz -C /opt/ \
    && rm /tmp/hadoop.tar.gz*

RUN ln -s /opt/hadoop-$HADOOP_VERSION/etc/hadoop /etc/hadoop \
    && mkdir /opt/hadoop-$HADOOP_VERSION/logs \
    && chmod -R 777 /opt/hadoop-$HADOOP_VERSION/logs \
    && mkdir /hadoop-data \
    && chmod -R 777 /hadoop-data \
    && ln -s /opt/hadoop-$HADOOP_VERSION /opt/hadoop \
    && ln -s $JAVA_HOME /opt/java

ENV HADOOP_PREFIX=/opt/hadoop-$HADOOP_VERSION 
ENV HADOOP_CONF_DIR=/etc/hadoop 
ENV MULTIHOMED_NETWORK=1 
ENV USER=root 
ENV PATH=$HADOOP_PREFIX/bin/:$HADOOP_PREFIX/sbin/:$PATH
ENV HADOOP_HOME=${HADOOP_PREFIX} 
COPY ETC/entrypoint.hadoop.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh && \
    echo "export PATH=$PATH" >> /home/vscode/.bashrc && \
    echo "export PATH=$PATH" >> /root/.bashrc && \
    mkdir -p /home/vscode/.ssh && \
    ssh-keygen -t rsa -P '' -f /root/.ssh/id_dsa && \
    ssh-keygen -t rsa -P '' -f /home/vscode/.ssh/id_dsa && \
    cat /root/.ssh/id_dsa.pub >> /root/.ssh/authorized_keys && \
    cat /home/vscode/.ssh/id_dsa.pub >> /home/vscode/.ssh/authorized_keys && \
    chown -R vscode:vscode /home/vscode/.ssh

# RUN sed -i "sexport PATH/g" /etc/nginx/nginx.conf
# ADD entrypoint.sh /entrypoint.sh
# RUN chmod a+x /entrypoint.sh
# ENTRYPOINT ["/entrypoint.sh"]


