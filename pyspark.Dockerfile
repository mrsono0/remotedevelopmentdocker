  
FROM mrsono0/devremotecontainers:anaconda3 as oraclejdk8

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
	gzip unzip zip \
	; \
	apt-get clean; \
	rm -rf /var/lib/apt/lists/*

COPY ETC/jdk-8u231-linux-x64.tar.gz /usr/local/
RUN cd /usr/local; tar -zxvf jdk-8u231-linux-x64.tar.gz; rm /usr/local/jdk-8u231-linux-x64.tar.gz ; cd /

ENV JAVA_HOME /usr/local/jdk1.8.0_231
ENV PATH $JAVA_HOME/bin:$PATH
RUN echo "export JAVA_HOME=${JAVA_HOME}" >> /etc/profile \
  && echo "export PATH=${JAVA_HOME}/bin:$PATH" >> /etc/profile

RUN pip install beakerx pandas py4j \
    && beakerx install \
    && cd /opt/conda/share/jupyter/kernels; rm -r clojure groovy scala sql

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/root"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN apt-get update && \
    apt-get install -y \
    procps \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
    #
    # Install git, process tools, lsb-release (common in install instructions for CLIs)
    && apt-get -y install procps lsb-release \
    #
    # Allow for a consistant java home location for settings - image is changing over time
    && if [ ! -d "/docker-java-home" ]; then ln -s "${JAVA_HOME}" /docker-java-home; fi \
    && if [ ! -d "/usr/local/default-jdk" ]; then ln -s "${JAVA_HOME}" /usr/local/default-jdk; fi \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
	
RUN wget http://apache.mirror.cdnetworks.com/tomcat/tomcat-9/v9.0.30/bin/apache-tomcat-9.0.30.tar.gz \
    && tar xzf apache-tomcat-9.0.30.tar.gz; rm -rf apache-tomcat-9.0.30.tar.gz \
    && mv apache-tomcat-9.0.30 /usr/local/tomcat9; chmod -R 775 /usr/local/tomcat9/; chown -R root:vscode /usr/local/tomcat9
ENV CATALINA_HOME /usr/local/tomcat9
ENV PATH ${CATALINA_HOME}/bin:$PATH
ENV TOMCAT_NATIVE_LIBDIR ${CATALINA_HOME}/native-jni-lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${TOMCAT_NATIVE_LIBDIR}

ENV KOTLIN_HOME /usr/lib/kotlinc
ENV PATH $PATH:$KOTLIN_HOME/bin
RUN echo "export KOTLIN_HOME=${KOTLIN_HOME}" >> /etc/profile
RUN echo "export PATH=${KOTLIN_HOME}/bin:$PATH" >> /etc/profile
ENV KOTLIN_VERSION=1.3.50
RUN cd /usr/lib && \
    wget https://github.com/JetBrains/kotlin/releases/download/v$KOTLIN_VERSION/kotlin-compiler-$KOTLIN_VERSION.zip && \
    unzip kotlin-compiler-*.zip && \
    rm kotlin-compiler-*.zip && \
    rm -f kotlinc/bin/*.bat

RUN unset JAVA_VERSION JAVA_BASE_URL JAVA_URL_VERSION

COPY ETC/*-java-*.vsix \
  ETC/*.java-*.vsix \
  ETC/vscjava.vscode-maven-0.20.0.vsix \
  ETC/adashen.vscode-tomcat-0.11.1.vsix \
  ETC/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  ETC/*-spring-*.vsix \
  /home/vscode/

RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
    && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
    && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER vscode
RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
    && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
    && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER root
RUN rm -f /home/vscode/*.vsix

FROM oraclejdk8

ARG BUILD_DATE
ARG SPARK_VERSION=2.4.4

ENV PYSPARK_PYTHON="/opt/conda/bin/python"

RUN apt-get update && \
    apt-get install -y curl bzip2 --no-install-recommends && \
    conda config --set auto_update_conda true && \
    conda config --set channel_priority false && \
    conda update conda -y -f && \
    conda install --quiet -y 'pyarrow' && \
    conda install -c conda-forge r r-essentials && \
    conda clean -tipsy && \
    echo "PATH=/opt/conda/bin:\${PATH}" > /etc/profile.d/conda.sh && \
    pip install --no-cache pyspark==${SPARK_VERSION} findspark && \
    SPARK_HOME=$(python /opt/conda/bin/find_spark_home.py) && \
    echo "export SPARK_HOME=$(python /opt/conda/bin/find_spark_home.py)" > /etc/profile.d/spark.sh && \
    echo "export PYSPARK_DRIVER_PYTHON=jupyter" >> /etc/profile.d/spark.sh && \
    echo "export PYSPARK_DRIVER_PYTHON_OPTS=lab" >> /etc/profile.d/spark.sh && \
    curl -s --url "http://central.maven.org/maven2/com/amazonaws/aws-java-sdk/1.7.4/aws-java-sdk-1.7.4.jar" --output $SPARK_HOME/jars/aws-java-sdk-1.7.4.jar && \
    curl -s --url "http://central.maven.org/maven2/org/apache/hadoop/hadoop-aws/2.7.3/hadoop-aws-2.7.3.jar" --output $SPARK_HOME/jars/hadoop-aws-2.7.3.jar && \
    mkdir -p $SPARK_HOME/conf && \
    echo "spark.hadoop.fs.s3.impl=org.apache.hadoop.fs.s3a.S3AFileSystem" >> $SPARK_HOME/conf/spark-defaults.conf && \
    apt-get autoremove -y && \
    apt-get clean

# ENTRYPOINT ["spark-submit"]
# CMD ["--help"]