FROM mrsono0/devremotecontainers:oraclejdk8

ENV DEBIAN_FRONTEND noninteractive
RUN sed -i "s/archive.ubuntu.com/mirror.kakao.com/g" /etc/apt/sources.list \
    && sed -i "s/security.ubuntu.com/mirror.kakao.com/g" /etc/apt/sources.list \
    && sed -i "s/# deb-src/deb-src/g" /etc/apt/sources.list
RUN apt-get -y update --fix-missing \
    && apt-get -yy upgrade \
    && apt-get install p7zip-full
USER root
COPY ETC/assets /assets
RUN chown -R root:root /assets \
    && chmod -R +x /assets/* \
    && /assets/setup.sh
# Clean up
RUN rm -rf /assets \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 1521
EXPOSE 8080

CMD /usr/sbin/startup.sh && tail -f /dev/null