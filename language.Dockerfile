FROM mrsono0/devremotecontainers:miniconda3 as oraclejdk11

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
	curl zip gzip unzip wget \
	; \
	apt-get clean; \
	rm -rf /var/lib/apt/lists/*

ENV JAVA_FILE jdk-11.0.5_linux-x64_bin.tar.gz
COPY ETC/${JAVA_FILE} /usr/local/
RUN cd /usr/local; tar -zxvf ${JAVA_FILE}; rm /usr/local/${JAVA_FILE}; ls -al; cd /

ENV JAVA_HOME /usr/local/jdk-11.0.5
ENV PATH $JAVA_HOME/bin:$PATH
# RUN update-alternatives --config java

# basic smoke test
RUN	javac -version; \
	java -version

## https://github.com/SpencerPark/IJava
RUN wget https://github.com/SpencerPark/IJava/releases/download/v1.3.0/ijava-1.3.0.zip
RUN mv ijava-1.3.0.zip /usr/local/; cd /usr/local; unzip ijava-1.3.0.zip; python /usr/local/install.py --sys-prefix; rm -r java install.py ijava-1.3.0.zip

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/root"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN apt-get update && \
    apt-get install -y \
      curl procps \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/local/maven /usr/local/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/local/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/local/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/local/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
    #
    # Install git, process tools, lsb-release (common in install instructions for CLIs)
    && apt-get -y install procps lsb-release \
    #
    # Allow for a consistant java home location for settings - image is changing over time
    && if [ ! -d "/docker-java-home" ]; then ln -s "${JAVA_HOME}" /docker-java-home; fi \
    && if [ ! -d "/usr/local/default-jdk" ]; then ln -s "${JAVA_HOME}" /usr/local/default-jdk; fi \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN wget http://apache.mirror.cdnetworks.com/tomcat/tomcat-9/v9.0.30/bin/apache-tomcat-9.0.30.tar.gz \
  && tar xzf apache-tomcat-9.0.30.tar.gz; rm -rf apache-tomcat-9.0.30.tar.gz \
  && mv apache-tomcat-9.0.30 /usr/local/tomcat9; chmod -R 775 /usr/local/tomcat9/; chown -R root:vscode /usr/local/tomcat9
ENV CATALINA_HOME /usr/local/tomcat9
ENV PATH ${CATALINA_HOME}/bin:$PATH
ENV TOMCAT_NATIVE_LIBDIR ${CATALINA_HOME}/native-jni-lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${TOMCAT_NATIVE_LIBDIR}

ENV KOTLIN_HOME /usr/local/kotlinc
ENV PATH $PATH:$KOTLIN_HOME/bin
RUN echo "export KOTLIN_HOME=${KOTLIN_HOME}" >> /etc/profile
RUN echo "export PATH=${KOTLIN_HOME}/bin:$PATH" >> /etc/profile
ENV KOTLIN_VERSION=1.3.61
RUN cd /usr/local && \
    wget https://github.com/JetBrains/kotlin/releases/download/v$KOTLIN_VERSION/kotlin-compiler-$KOTLIN_VERSION.zip && \
    unzip kotlin-compiler-*.zip && \
    rm kotlin-compiler-*.zip && \
    rm -f kotlinc/bin/*.bat

RUN unset JAVA_VERSION JAVA_BASE_URL JAVA_URL_VERSION

COPY ETC/vscjava.vscode-java-pack-0.8.1.vsix \
  ETC/redhat.java-0.53.1.vsix \
  ETC/vscjava.vscode-java-debug-0.23.0.vsix \
  ETC/vscjava.vscode-java-dependency-0.6.0.vsix \
  ETC/vscjava.vscode-java-test-0.21.0.vsix \
  ETC/vscjava.vscode-maven-0.20.0.vsix \
  ETC/adashen.vscode-tomcat-0.11.1.vsix \
  ETC/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  ETC/Pivotal.vscode-spring-boot-1.13.0.vsix \
  ETC/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
  ETC/vscjava.vscode-spring-initializr-0.4.6.vsix \
  /home/vscode/

RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
  && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
  && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
  && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER vscode
RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
  && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
  && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
  && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER root
RUN rm -f /home/vscode/*.vsix

FROM oraclejdk11 as nodejs

ENV NODE_VERSION 13.6.0

RUN ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  # gpg keys listed at https://github.com/nodejs/node#release-keys
  && set -ex \
  && for key in \
    94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
    FD3A5288F042B6850C66B31F09FE44734EB7990E \
    71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
    DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
    B9AE9905FFD7803F25714661B63B535A4C206CA9 \
    77984A986EBC2AA786BC0F66B01FBB92821C587A \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
    4ED778F539E3634C779C87C6D7062848A1AB005C \
    A48C2BEE680E841632CD4E44F07496B3EB3C1762 \
    B9E2F5981AA6E0CD28160D9FF13993A75599653C \
  ; do \
    gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" || \
    gpg --batch --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" || \
    gpg --batch --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && grep " node-v$NODE_VERSION-linux-$ARCH.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs

# ENV YARN_VERSION 1.21.1

# RUN set -ex \
#   && for key in \
#     6A010C5166006599AA17F08146C2130DFD2497F5 \
#   ; do \
#     gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" || \
#     gpg --batch --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" || \
#     gpg --batch --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ; \
#   done \
#   && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" \
#   && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz.asc" \
#   && gpg --batch --verify yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz \
#   && mkdir -p /opt \
#   && tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/ \
#   && ln -s /opt/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn \
#   && ln -s /opt/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg \
#   && rm yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz

## https://dydals5678.tistory.com/89
## https://github.com/yunabe/tslab
RUN npm install --silent --save-dev -g \
        gulp-cli \
        typescript \
		@babel/node \
		@babel/core \
		tslab
RUN tslab install
RUN jupyter kernelspec list

# Configure apt and install packages
RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \ 
    #
    # Verify git and needed tools are installed
    && apt-get install -y git procps \
    #
    # Install eslint globally
    && npm install -g eslint \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

FROM nodejs as R

RUN conda install --quiet --yes \
            'r-base' \
            'r-irkernel' \
            'r-plyr' \
            'r-devtools' \
            'r-tidyverse' \
            'r-shiny' \
            'r-rmarkdown' \
            'r-forecast' \
            'r-rsqlite' \
            'r-reshape2' \
            'r-nycflights13' \
            'r-caret' \
            'r-rcurl' \
            'r-crayon' \
            'r-randomforest' \
            'r-tensorflow'

FROM R

# make some useful symlinks that are expected to exist
RUN cd /usr/local/bin \
	&& ln -s idle3 idle \
	&& ln -s pydoc3 pydoc \
	&& ln -s python3 python \
	&& ln -s python3-config python-config

# if this is called "PIP_VERSION", pip explodes with "ValueError: invalid truth value '<VERSION>'"
ENV PYTHON_PIP_VERSION 19.3.1
# https://github.com/pypa/get-pip
ENV PYTHON_GET_PIP_URL https://github.com/pypa/get-pip/raw/ffe826207a010164265d9cc807978e3604d18ca0/get-pip.py
ENV PYTHON_GET_PIP_SHA256 b86f36cc4345ae87bfd4f10ef6b2dbfa7a872fbff70608a1e43944d283fd0eee

RUN set -ex; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	apt-get update; \
	apt-get install -y --no-install-recommends wget; \
	\
	wget -O get-pip.py "$PYTHON_GET_PIP_URL"; \
	echo "$PYTHON_GET_PIP_SHA256 *get-pip.py" | sha256sum --check --strict -; \
	\
	apt-mark auto '.*' > /dev/null; \
	[ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	rm -rf /var/lib/apt/lists/*; \
	\
	python get-pip.py \
	--disable-pip-version-check \
	--no-cache-dir \
	"pip==$PYTHON_PIP_VERSION" \
	; \
	pip --version; \
	\
	find /usr/local -depth \
	\( \
	\( -type d -a \( -name test -o -name tests -o -name idle_test \) \) \
	-o \
	\( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
	\) -exec rm -rf '{}' +; \
	rm -f get-pip.py

RUN bash -c "pip install --no-input pylint"

RUN apt-get update && apt-get install -y --no-install-recommends \
		ca-certificates \
		curl \
		netbase \
		wget \
		git \
		mercurial \
		openssh-client \
		subversion \
		\
		procps \
	&& rm -rf /var/lib/apt/lists/*

RUN set -ex; \
	if ! command -v gpg > /dev/null; then \
		apt-get update; \
		apt-get install -y --no-install-recommends \
			gnupg \
			dirmngr \
		; \
		rm -rf /var/lib/apt/lists/*; \
	fi

RUN set -ex; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		autoconf \
		automake \
		bzip2 \
		dpkg-dev \
		file \
		g++ \
		gcc \
		imagemagick \
		libbz2-dev \
		libc6-dev \
		libcurl4-openssl-dev \
		libdb-dev \
		libevent-dev \
		libffi-dev \
		libgdbm-dev \
		libglib2.0-dev \
		libgmp-dev \
		libjpeg-dev \
		libkrb5-dev \
		liblzma-dev \
		libmagickcore-dev \
		libmagickwand-dev \
		libmaxminddb-dev \
		libncurses5-dev \
		libncursesw5-dev \
		libpng-dev \
		libpq-dev \
		libreadline-dev \
		libsqlite3-dev \
		libssl-dev \
		libtool \
		libwebp-dev \
		libxml2-dev \
		libxslt-dev \
		libyaml-dev \
		make \
		patch \
		unzip \
		xz-utils \
		zlib1g-dev \
        libzmq3-dev \
		\
# https://lists.debian.org/debian-devel-announce/2016/09/msg00000.html
		$( \
# if we use just "apt-cache show" here, it returns zero because "Can't select versions from package 'libmysqlclient-dev' as it is purely virtual", hence the pipe to grep
			if apt-cache show 'default-libmysqlclient-dev' 2>/dev/null | grep -q '^Version:'; then \
				echo 'default-libmysqlclient-dev'; \
			else \
				echo 'libmysqlclient-dev'; \
			fi \
		) \
	; \
	rm -rf /var/lib/apt/lists/*

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/jdk-11.0.5/bin:/usr/local/cargo/bin:/usr/local/rustup:$PATH \
    RUST_VERSION=1.40.0 

RUN set -eux; \
    dpkgArch="$(dpkg --print-architecture)"; \
    case "${dpkgArch##*-}" in \
        amd64) rustArch='x86_64-unknown-linux-gnu'; rustupSha256='e68f193542c68ce83c449809d2cad262cc2bbb99640eb47c58fc1dc58cc30add' ;; \
        armhf) rustArch='armv7-unknown-linux-gnueabihf'; rustupSha256='7c1c329a676e50c287d8183b88f30cd6afd0be140826a9fbbc0e3d717fab34d7' ;; \
        arm64) rustArch='aarch64-unknown-linux-gnu'; rustupSha256='d861cc86594776414de001b96964be645c4bfa27024052704f0976dc3aed1b18' ;; \
        i386) rustArch='i686-unknown-linux-gnu'; rustupSha256='89f1f797dca2e5c1d75790c3c6b7be0ee473a7f4eca9663e623a41272a358da0' ;; \
        *) echo >&2 "unsupported architecture: ${dpkgArch}"; exit 1 ;; \
    esac; \
    url="https://static.rust-lang.org/rustup/archive/1.20.2/${rustArch}/rustup-init"; \
    wget "$url"; \
    echo "${rustupSha256} *rustup-init" | sha256sum -c -; \
    chmod +x rustup-init; \
    ./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION; \
    rm rustup-init; \
    chmod -R a+w $RUSTUP_HOME $CARGO_HOME; \
    rustup --version; \
    cargo --version; \
    rustc --version; \
    cargo install evcxr_jupyter --no-default-features; \
    evcxr_jupyter --install; \
	chmod -R 777 /usr/local/cargo/registry;

RUN unset RUST_VERSION MAVEN_CONFIG && \
    echo "export PATH=$PATH" >> /home/vscode/.bashrc && \
	echo "export USER=vscode" >> /home/vscode/.bashrc && \
    echo "export PATH=$PATH" >> /root/.bashrc && \
	echo "export USER=root" >> /root/.bashrc && \
	rustup update && \
	rustup component add rls rust-analysis rust-src 

COPY ETC/rust-lang.rust-0.7.0.vsix /home/vscode/
RUN code-server --install-extension /home/vscode/rust-lang.rust-0.7.0.vsix
USER vscode
RUN code-server --install-extension /home/vscode/rust-lang.rust-0.7.0.vsix
USER root
RUN rm -f /home/vscode/*.vsix

RUN conda install --quiet --yes \
    'jupyterhub' && \
    # conda clean -tipsy && \
    # jupyter labextension install @jupyterlab/hub-extension@^0.12.0 && \
    # npm cache clean --force && \
    jupyter notebook --generate-config

RUN pip3 install --no-cache-dir --upgrade \
    numpy \
    pandas \
    pandas-datareader \
    pandas-td \
    ipywidgets \
    jupyter_dashboards \
    pypki2 \
    ipydeps \
    ordo \
    beakerx \
    bash_kernel 
RUN conda install -c conda-forge \
    cling \
    xeus-cling \
    py4j
RUN beakerx install
RUN python3 -m bash_kernel.install
# RUN npm config set user 0 && \
#     npm config set unsafe-perm true && \
#     npm install -g ijavascript && \
#     ijsinstall
# RUN npm install -g itypescript && \
#     its --ts-install=local
RUN pip3 install http://github.com/nbgallery/nbgallery-extensions/tarball/master#egg=jupyter_nbgallery && \
    echo "### Install jupyter extensions" && \
    jupyter serverextension enable --py jupyterlab && \
    jupyter nbextension enable --py --sys-prefix widgetsnbextension && \
    jupyter serverextension enable --py jupyter_nbgallery && \
    jupyter nbextension install --py jupyter_nbgallery && \
    jupyter nbextension enable jupyter_nbgallery --py && \
    jupyter dashboards quick-setup --sys-prefix && \
    jupyter nbextension install --py ordo && \
    jupyter nbextension enable ordo --py

USER vscode
RUN evcxr_jupyter --install
USER root

RUN rm -rf /opt/conda/share/jupyter/kernels/clojure && \
    rm -rf /opt/conda/share/jupyter/kernels/groovy && \
    rm -rf /opt/conda/share/jupyter/kernels/scala && \
    rm -rf /opt/conda/share/jupyter/kernels/sql && \
    rm -rf /opt/conda/share/jupyter/kernels/xcpp11 
