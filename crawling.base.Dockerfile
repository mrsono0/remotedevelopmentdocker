FROM mrsono0/devremotecontainers:anaconda3 as oraclejdk8

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
	gzip unzip zip \
	; \
	apt-get clean; \
	rm -rf /var/lib/apt/lists/*

COPY ETC/jdk-8u231-linux-x64.tar.gz /usr/local/
RUN cd /usr/local; tar -zxvf jdk-8u231-linux-x64.tar.gz; rm /usr/local/jdk-8u231-linux-x64.tar.gz ; cd /

ENV JAVA_HOME /usr/local/jdk1.8.0_231
ENV PATH $JAVA_HOME/bin:$PATH
RUN echo "export JAVA_HOME=${JAVA_HOME}" >> /etc/profile \
  && echo "export PATH=${JAVA_HOME}/bin:$PATH" >> /etc/profile

RUN pip install beakerx pandas py4j \
    && beakerx install \
    && cd /opt/conda/share/jupyter/kernels; rm -r clojure groovy scala sql

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/root"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN apt-get update && \
    apt-get install -y \
    procps \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
    #
    # Install git, process tools, lsb-release (common in install instructions for CLIs)
    && apt-get -y install procps lsb-release \
    #
    # Allow for a consistant java home location for settings - image is changing over time
    && if [ ! -d "/docker-java-home" ]; then ln -s "${JAVA_HOME}" /docker-java-home; fi \
    && if [ ! -d "/usr/local/default-jdk" ]; then ln -s "${JAVA_HOME}" /usr/local/default-jdk; fi \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
	
RUN wget http://apache.mirror.cdnetworks.com/tomcat/tomcat-9/v9.0.30/bin/apache-tomcat-9.0.30.tar.gz \
    && tar xzf apache-tomcat-9.0.30.tar.gz; rm -rf apache-tomcat-9.0.30.tar.gz \
    && mv apache-tomcat-9.0.30 /usr/local/tomcat9; chmod -R 775 /usr/local/tomcat9/; chown -R root:vscode /usr/local/tomcat9
ENV CATALINA_HOME /usr/local/tomcat9
ENV PATH ${CATALINA_HOME}/bin:$PATH
ENV TOMCAT_NATIVE_LIBDIR ${CATALINA_HOME}/native-jni-lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${TOMCAT_NATIVE_LIBDIR}

ENV KOTLIN_HOME /usr/lib/kotlinc
ENV PATH $PATH:$KOTLIN_HOME/bin
RUN echo "export KOTLIN_HOME=${KOTLIN_HOME}" >> /etc/profile
RUN echo "export PATH=${KOTLIN_HOME}/bin:$PATH" >> /etc/profile
ENV KOTLIN_VERSION=1.3.50
RUN cd /usr/lib && \
    wget https://github.com/JetBrains/kotlin/releases/download/v$KOTLIN_VERSION/kotlin-compiler-$KOTLIN_VERSION.zip && \
    unzip kotlin-compiler-*.zip && \
    rm kotlin-compiler-*.zip && \
    rm -f kotlinc/bin/*.bat

RUN unset JAVA_VERSION JAVA_BASE_URL JAVA_URL_VERSION

COPY ETC/*-java-*.vsix \
  ETC/*.java-*.vsix \
  ETC/vscjava.vscode-maven-0.20.0.vsix \
  ETC/adashen.vscode-tomcat-0.11.1.vsix \
  ETC/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  ETC/*-spring-*.vsix \
  /home/vscode/

RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
    && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
    && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER vscode
RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
    && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
    && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
    && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
    && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER root
RUN rm -f /home/vscode/*.vsix

FROM oraclejdk8 as crawling_base

# Install dependencies.
RUN apt-get update
RUN apt-get install -y xvfb unzip libxi6 libgconf-2-4 gnupg gnupg2 gnupg1 \
    p11-kit \
    sudo \
    unzip \
    wget \
    jq \
    curl \
    supervisor \
    gnupg2 \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*
#   && sed -i 's/securerandom\.source=file:\/dev\/random/securerandom\.source=file:\/dev\/urandom/' ./usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/java.security

COPY ETC/entrypoint.eda.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
COPY ETC/supervisord.conf /etc

#==========
# Selenium & relaxing permissions for OpenShift and other non-sudo environments
#==========
RUN  mkdir -p /opt/selenium /var/run/supervisor /var/log/supervisor \
  && touch /opt/selenium/config.json \
  && chmod -R 777 /opt/selenium /var/run/supervisor /var/log/supervisor /etc/passwd \
  && wget --no-verbose https://selenium-release.storage.googleapis.com/3.141/selenium-server-standalone-3.141.59.jar \
    -O /opt/selenium/selenium-server-standalone.jar \
  && chgrp -R 0 /opt/selenium ${HOME} /var/run/supervisor /var/log/supervisor \
  && chmod -R g=u /opt/selenium ${HOME} /var/run/supervisor /var/log/supervisor

#==============
# Xvfb
#==============
RUN apt-get update -qqy \
  && apt-get -qqy install \
    xvfb \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

#================
# Font libraries
#================
# libfontconfig            ~1 MB
# libfreetype6             ~1 MB
# xfonts-cyrillic          ~2 MB
# xfonts-scalable          ~2 MB
# fonts-liberation         ~3 MB
# fonts-ipafont-gothic     ~13 MB
# fonts-wqy-zenhei         ~17 MB
# fonts-tlwg-loma-otf      ~300 KB
# ttf-ubuntu-font-family   ~5 MB
#   Ubuntu Font Family, sans-serif typeface hinted for clarity
# Removed packages:
# xfonts-100dpi            ~6 MB
# xfonts-75dpi             ~6 MB
# Regarding fonts-liberation see:
#  https://github.com/SeleniumHQ/docker-selenium/issues/383#issuecomment-278367069
# Layer size: small: 36.28 MB (with --no-install-recommends)
# Layer size: small: 36.28 MB
RUN apt-get -qqy update \
  && apt-get -qqy --no-install-recommends install \
    libfontconfig \
    libfreetype6 \
    xfonts-cyrillic \
    xfonts-scalable \
    fonts-liberation \
    fonts-ipafont-gothic \
    fonts-wqy-zenhei \
    fonts-tlwg-loma-otf \
	fonts-nanum \
	# ttf-nanum ttf-nanum-coding \
    # ttf-ubuntu-font-family \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get -qyy clean

#==============================
# Scripts to run Selenium Node and XVFB
#==============================
COPY ETC/start-selenium-node.sh \
      ETC/start-xvfb.sh \
      /usr/local/bin/
RUN chmod +x /usr/local/bin/start-selenium-node.sh \
    && chmod +x /usr/local/bin/start-xvfb.sh

#==============================
# Supervisor configuration file
#==============================
COPY ETC/selenium.conf /etc/supervisor/conf.d/


#============================
# Some configuration options
#============================
ENV SCREEN_WIDTH 1360
ENV SCREEN_HEIGHT 1020
ENV SCREEN_DEPTH 24
ENV SCREEN_DPI 96
ENV DISPLAY :99.0
ENV START_XVFB true

#========================
# Selenium Configuration
#========================
# As integer, maps to "maxInstances"
ENV NODE_MAX_INSTANCES 1
# As integer, maps to "maxSession"
ENV NODE_MAX_SESSION 1
# As address, maps to "host"
ENV NODE_HOST 0.0.0.0
# As integer, maps to "port"
ENV NODE_PORT 5555
# In milliseconds, maps to "registerCycle"
ENV NODE_REGISTER_CYCLE 5000
# In milliseconds, maps to "nodePolling"
ENV NODE_POLLING 5000
# In milliseconds, maps to "unregisterIfStillDownAfter"
ENV NODE_UNREGISTER_IF_STILL_DOWN_AFTER 60000
# As integer, maps to "downPollingLimit"
ENV NODE_DOWN_POLLING_LIMIT 2
# As string, maps to "applicationName"
ENV NODE_APPLICATION_NAME ""
# Debug
ENV GRID_DEBUG false

# Following line fixes https://github.com/SeleniumHQ/docker-selenium/issues/87
ENV DBUS_SESSION_BUS_ADDRESS=/dev/null

# Creating base directory for Xvfb
RUN mkdir -p /tmp/.X11-unix && sudo chmod 1777 /tmp/.X11-unix

RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*