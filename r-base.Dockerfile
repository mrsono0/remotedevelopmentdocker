FROM mrsono0/devremotecontainers:oraclejdk8

# https://github.com/rocker-org/rocker/blob/df56b98e4a2a4611fa9aacae99c4a304531c2640/r-base/Dockerfile
RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		ed \
		less \
		locales \
		vim-tiny \
		wget \
		ca-certificates \
		fonts-texgyre 

## Use Debian unstable via pinning -- new style via APT::Default-Release
RUN echo "deb http://http.debian.net/debian sid main" > /etc/apt/sources.list.d/debian-unstable.list \
        && echo 'APT::Default-Release "testing";' > /etc/apt/apt.conf.d/default

ENV R_BASE_VERSION 3.6.2

## Now install R and littler, and create a link for littler in /usr/local/bin
RUN apt-get update \
	&& apt-get install -t unstable -y --no-install-recommends \
		littler \
        r-cran-littler \
		r-base=${R_BASE_VERSION}-* \
		r-base-dev=${R_BASE_VERSION}-* \
		r-recommended=${R_BASE_VERSION}-* \
	&& ln -s /usr/lib/R/site-library/littler/examples/install.r /usr/local/bin/install.r \
	&& ln -s /usr/lib/R/site-library/littler/examples/install2.r /usr/local/bin/install2.r \
	&& ln -s /usr/lib/R/site-library/littler/examples/installGithub.r /usr/local/bin/installGithub.r \
	&& ln -s /usr/lib/R/site-library/littler/examples/testInstalled.r /usr/local/bin/testInstalled.r \
	&& install.r docopt \
	&& rm -rf /tmp/downloaded_packages/ /tmp/*.rds \
	&& rm -rf /var/lib/apt/lists/*

RUN echo "install.packages(c('repr', 'IRdisplay', 'RSentiment', 'IRkernel', 'igraph'), repos='https://mirror.las.iastate.edu/CRAN')" | R --vanilla
RUN echo "IRkernel::installspec(user = FALSE)" | R --vanilla
# RUN echo "setRepositories(ind=c(2)); source('https://bioconductor.org/biocLite.R'); biocLite('gRain',suppressUpdates=TRUE,ask=FALSE);biocLite('Rgraphviz',suppressUpdates=TRUE,ask=FALSE);" | R --vanilla
# Install R modules
RUN R -e "install.packages('rjson')" && \
	R -e "install.packages('XML')" && \
	R -e "install.packages('xml2')" && \
	R -e "install.packages('charlatan')" && \
	R -e "install.packages('httpuv')" && \
	R -e "install.packages('curl')" && \
	R -e "install.packages('httr')" && \
	R -e "install.packages('shiny')" && \
	R -e "install.packages('rmarkdown')" && \
	R -e "install.packages('knitr')" && \
	R -e "install.packages('caTools')" && \
	R -e "install.packages('writexl')" && \
	R -e "install.packages('rlist')" && \
	R -e "install.packages('tictoc')"&& \
	R -e "install.packages('kableExtra')"
# Install R packages
RUN R CMD javareconf && R -e "install.packages('rJava')" && \
	R -e "install.packages('odbc')" && \
	R -e "install.packages('RJDBC')"
# RUN echo "PATH=$PATH" >> ~/.bashrc

# COPY ETC/Mikhail-Arkhipov.r-0.0.6.vsix \
# 	 ETC/Ikuyadeu.r-1.1.8.vsix \
# 	 /home/vscode/
# RUN code-server --install-extension /home/vscode/Mikhail-Arkhipov.r-0.0.6.vsix \
# 	&& code-server --install-extension /home/vscode/Ikuyadeu.r-1.1.8.vsix
# USER vscode
# RUN code-server --install-extension /home/vscode/Mikhail-Arkhipov.r-0.0.6.vsix \
# 	&& code-server --install-extension /home/vscode/Ikuyadeu.r-1.1.8.vsix
# USER root
# RUN rm -f /home/vscode/*.vsix