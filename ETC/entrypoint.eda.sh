#!/usr/bin/env bash

if ! whoami &> /dev/null; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:${HOME}:/sbin/nologin" >> /etc/passwd
  fi
fi

/usr/bin/supervisord --configuration /etc/supervisord.conf &

SUPERVISOR_PID=$!

function shutdown {
    echo "Trapped SIGTERM/SIGINT/x so shutting down supervisord..."
    kill -s SIGTERM ${SUPERVISOR_PID}
    wait ${SUPERVISOR_PID}
    echo "Shutdown complete"
}

trap shutdown SIGTERM SIGINT
# wait ${SUPERVISOR_PID}

dumb-init code-server --host 0.0.0.0 --auth none --port 8889 &

if [[ "${JUPYTER_RUN}" = "yes" ]]; then
    mkdir -p /home/vscode/notebooks
    chown -R vscode /home/vscode/notebooks 
   dumb-init jupyter lab --notebook-dir=/home/vscode/notebooks --ip='*' --port=8888 --no-browser --allow-root &
fi

exec "$@"
