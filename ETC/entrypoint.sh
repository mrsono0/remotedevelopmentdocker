#!/bin/bash

# code-server --install-extension /home/vscode/donjayamanne.python-extension-pack-1.6.0.vsix
# code-server --install-extension /home/vscode/ms-python.python-2019.11.50794.vsix
# code-server --install-extension /home/vscode/ms-python.anaconda-extension-pack-1.0.1.vsix 
# code-server --install-extension /home/vscode/MS-CEINTL.vscode-language-pack-ko-1.40.2.vsix /home/vscode
# code-server --install-extension /home/vscode/vscode-icons-team.vscode-icons-9.6.0.vsix
# rm -f /home/vscode/vscode-icons-team.vscode-icons-9.6.0.vsix

dumb-init code-server --host 0.0.0.0 --auth none --port 8889 &

exec "$@"
