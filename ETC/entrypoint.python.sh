#!/bin/bash

dumb-init code-server --host 0.0.0.0 --auth none --port 8889 &

if [[ "${JUPYTER_RUN}" = "yes" ]]; then
    mkdir -p /home/vscode/notebooks 
   dumb-init jupyter lab --notebook-dir=/home/vscode/notebooks --ip='*' --port=8888 --no-browser --allow-root &
fi

exec "$@"
