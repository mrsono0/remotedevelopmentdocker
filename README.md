# Developing inside a Container

The Visual Studio Code Remote - Containers extension lets you use a Docker container as a full-featured development environment

## Python

- ### Anaconda3 2019.10

```
docker build -f anaconda3.Dockerfile --tag mrsono0/devremotecontainers:anaconda3 .

docker run --rm --name anaconda3 -itd mrsono0/devremotecontainers:anaconda3

docker run --rm --name anaconda3 -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:anaconda3

docker run --rm --name anaconda3 -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -v ~:/home/vscode/prj --net=container:ora11xe --link ora11xe -e JUPYTER_RUN=yes mrsono0/devremotecontainers:anaconda3

docker push mrsono0/devremotecontainers:anaconda3
```

- ### Miniconda3 4.7.12

```
docker build -f miniconda3.Dockerfile --tag mrsono0/devremotecontainers:miniconda3 .

docker run --rm --name miniconda3 -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:miniconda3

docker push mrsono0/devremotecontainers:miniconda3
```


- ### Python 3.7

```
docker build -f python3.7.Dockerfile --tag mrsono0/devremotecontainers:python3.7 .

docker run --rm --name python3.7 -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:python3.7

docker push mrsono0/devremotecontainers:python3.7

python3 -m pip install --upgrade pip
```


- ### Python 3.8

```
docker build -f python3.8.Dockerfile --tag mrsono0/devremotecontainers:python3.8 .

docker run --rm --name python3.8 -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:python3.8

docker push mrsono0/devremotecontainers:python3.8
```

## Java

- ### OpenJDK8

```
docker build -f openjdk8.Dockerfile --tag mrsono0/devremotecontainers:openjdk8 .

docker run --rm --name openjdk8 -itd -u vscode -p 8080:8080 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:openjdk8

docker push mrsono0/devremotecontainers:openjdk8
```

- ### OpenJDK11

```
docker build -f openjdk11.Dockerfile --tag mrsono0/devremotecontainers:openjdk11 .

docker run --rm --name openjdk11 -itd -u vscode -p 8080:8080 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:openjdk11

docker push mrsono0/devremotecontainers:openjdk11
```

- ### OpenJDK13

```
docker build -f openjdk13.Dockerfile --tag mrsono0/devremotecontainers:openjdk13 .

docker run --rm --name openjdk13 -itd -u vscode -p 8080:8080 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:openjdk13

docker push mrsono0/devremotecontainers:openjdk13
```

- ### OpenJDK15

```
docker build -f openjdk15.Dockerfile --tag mrsono0/devremotecontainers:openjdk15 .

docker run --rm --name openjdk15 -itd -u vscode -p 8080:8080 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:openjdk15

docker push mrsono0/devremotecontainers:openjdk15
```

- ### Oracle JDK 8

```
docker build -f oraclejdk8.Dockerfile --tag mrsono0/devremotecontainers:oraclejdk8 .

docker run --rm --name oraclejdk8 -itd -u vscode -p 8080:8080 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:oraclejdk8

docker push mrsono0/devremotecontainers:oraclejdk8
```

- ### Oracle JDK 11

```
docker build -f oraclejdk11.Dockerfile --tag mrsono0/devremotecontainers:oraclejdk11 .

docker run --rm --name oraclejdk11 -itd -u vscode -p 8080:8080 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:oraclejdk11

docker push mrsono0/devremotecontainers:oraclejdk11
```

## Node.js

- ### node.js 12

```
docker build -f nodejs12.Dockerfile --tag mrsono0/devremotecontainers:nodejs12 .

docker run --rm --name nodejs12 -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:nodejs12

docker push mrsono0/devremotecontainers:nodejs12
```

- ### node.js 13

```
docker build -f nodejs13.Dockerfile --tag mrsono0/devremotecontainers:nodejs13 .

docker run --rm --name nodejs13 -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:nodejs13

docker push mrsono0/devremotecontainers:nodejs13
```

## R-base

- ### R 3.6.2

```
docker build -f r-base.Dockerfile --tag mrsono0/devremotecontainers:r-base .

docker run --rm --name r-base -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:r-base

docker push mrsono0/devremotecontainers:r-base
```

## AI

- ### Crawling Chrome for Selenium

```
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
cap = DesiredCapabilities.CHROME
driver = webdriver.Remote(command_executor='localhost:4444', desired_capabilities=cap)


docker build -f crawling.Dockerfile --tag mrsono0/devremotecontainers:crawling .

docker run --rm --name crawling -itd -u vscode -p 4444:4444 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:crawling

docker push mrsono0/devremotecontainers:crawling
```

- ### EDA (numpy, pandas, ... anaconda3)

```
docker build -f eda.Dockerfile --tag mrsono0/devremotecontainers:eda .

docker run --rm --name eda -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:eda

for Mac -v option
docker run --rm --name eda -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes -v /Users/mrsono0/OneDrive/MacBookPro/PNU_201912/5.탐색기반데이터분석기법:/home/vscode/notebooks mrsono0/devremotecontainers:eda

for windows -v option
docker run --rm --name eda -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes -v /C/Users/user/PNU_201912/eda:/home/vscode/notebooks/eda mrsono0/devremotecontainers:eda

docker push mrsono0/devremotecontainers:eda
```

- ### ai and tensorflow-cpu==1.15.0

```
docker build -f ai.Dockerfile --tag mrsono0/devremotecontainers:ai .

docker run --rm --name ai -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:ai

docker push mrsono0/devremotecontainers:ai
```

- ### ai and tensorflow-cpu==2.1.0

```
docker build -f tf2.Dockerfile --tag mrsono0/devremotecontainers:tf2 .

docker run --rm --name tf2 -itd -u vscode -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:tf2

docker push mrsono0/devremotecontainers:tf2
```

- ### Hadoop 3.1.2

1. hadoop-env.sh 파일에 아래 내용 수정  

```
vi /opt/hadoop-3.1.3/etc/hadoop/hadoop-env.sh
export JAVA_HOME=/usr/local/jdk-11.0.5
```

2. namenode format  

```
hadoop namenode -format
```

Run `docker network inspect` on the network (e.g. `dockerhadoop_default`) to find the IP the hadoop interfaces are published on.  
Access these interfaces with the following URLs:

    * Namenode: http://<dockerhadoop_IP_address>:9870/dfshealth.html#tab-overview
    * History server: http://<dockerhadoop_IP_address>:8188/applicationhistory
    * Datanode: http://<dockerhadoop_IP_address>:9864/
    * Nodemanager: http://<dockerhadoop_IP_address>:8042/node
    * Resource manager: http://<dockerhadoop_IP_address>:8088/

```
docker build -f hadoop3.1.2.Dockerfile --tag mrsono0/devremotecontainers:hadoop3.1.2 .

docker run --rm --name hadoop -itd -u vscode -p 9000:9000 -p 9870:9870 -p 9864:9864 -p8042:8042 -p 8088:8088 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:hadoop3.1.2

docker push mrsono0/devremotecontainers:hadoop3.1.2
```

- ### Spark 2.4.4

스파크 Standalone  

1. Start_Master (7077번 포트)  
    http://localhost:8080 Master 프로세스 확인

```
SPARK_HOME/sbin/start-master.sh  
```

2. Setup_Worker_Configuration

```
cd SPARK_HOME/conf
cp spark-env.sh.template spark-env.sh

# spark-env.sh 파일 수정
export SPARK_WORKER_INSTANCES=3
```

3. Start_Worker
 "-m" 은 워커가 사용할 메모리 지정
 "-c" 은 워커가 사용할 코어 개수 지정
 "spark://" 값은 Master Web UI에 최상단에 URL이라는 항목에 있는 값입니다.

```
SPARK_HOME/start-slave.sh spark://your_host_name:7077
SPARK_HOME/start-slave.sh spark://your_host_name:7077 -m 512M -c 1


docker build -f spark_hadoop.Dockerfile --tag mrsono0/devremotecontainers:spark_hadoop .

docker run --rm --name spark -itd -u vscode -p 9000:9000 -p 9870:9870 -p 9864:9864 -p8042:8042 -p 8088:8088 -p 7077:7077 -p 8080:8080 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:spark_hadoop

docker push mrsono0/devremotecontainers:spark_hadoop
```

## Rust

```
docker build -f rust1.40.0.Dockerfile --tag mrsono0/devremotecontainers:rust1.40.0 .

docker run --rm --name rust -itd -u vscode -p 8000:8000 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:rust1.40.0

docker push mrsono0/devremotecontainers:rust1.40.0
```

## All Computer Languages

```
docker build -f language.Dockerfile --tag mrsono0/devremotecontainers:language .

docker run --rm --name language -itd -u vscode -p 8000:8000 -p 8888-8889:8888-8889 -p 6006-6015:6006-6015 -e JUPYTER_RUN=yes mrsono0/devremotecontainers:language

docker push mrsono0/devremotecontainers:language
```

## vscode server

```
docker build -f vscode-server.Dockerfile --tag mrsono0/devremotecontainers:vscode-server .

docker run --rm --name vscode-server -it mrsono0/devremotecontainers:vscode-server

docker run --rm -itd --name vscode -u vscode -p 8889:8889 -p 6006-6015:6006-6015 -v ~/docker:/home/vscode/docker mrsono0/devremotecontainers:vscode-server

docker run --rm -itd --name vscode -u vscode -p 8889:8889 -p 6006-6015:6006-6015 mrsono0/devremotecontainers:vscode-server

docker push mrsono0/devremotecontainers:vscode-server
```

## Oracle XE 11G apex (웹 관리 기능)

```
    apex Login http://localhost:8080/apex/apex_admin with following credential:
    username: ADMIN
    password: admin
    Login http://localhost:8080/apex/apex_admin with following credential:
    username: ADMIN
    password: Oracle_11g

    By default, the password verification is disable(password never expired)<br/>
    Connect database with following setting:
    hostname: localhost
    port: 1521
    sid: xe
    username: system
    password: oracle

    Password for SYS & SYSTEM : oracle

    alter user hr account unlock;
    alter user hr identified by hr;

docker build -f oraxe11g.Dockerfile --tag mrsono0/devremotecontainers:oraxe11g .

docker run --rm --name oraxe11g -itd -p 8080:8088 -p 1521:1521 -e ORACLE_ALLOW_REMOTE=true mrsono0/devremotecontainers:oraxe11g

docker push mrsono0/devremotecontainers:oraxe11g

docker build -f oraexpress18c.Dockerfile --tag mrsono0/devremotecontainers:oraexpress18c .

docker run --rm --name oraexpress18c -itd -p 8080:8088 -p 1521:1521 -e ORACLE_ALLOW_REMOTE=true mrsono0/devremotecontainers:oraexpress18c

docker push mrsono0/devremotecontainers:oraexpress18c
```

## Developing inside a Container

```
git clone https://github.com/Microsoft/vscode-remote-try-node
git clone https://github.com/Microsoft/vscode-remote-try-python
git clone https://github.com/Microsoft/vscode-remote-try-go
git clone https://github.com/Microsoft/vscode-remote-try-java
git clone https://github.com/Microsoft/vscode-remote-try-dotnetcore
git clone https://github.com/Microsoft/vscode-remote-try-php
git clone https://github.com/Microsoft/vscode-remote-try-rust
git clone https://github.com/Microsoft/vscode-remote-try-cpp
```

## git large file include

```
brew install git-lfs
git lfs install
git lfs track “*.exe”
java -jar bfg-1.13.0.jar --strip-blobs-bigger-than 100M
git commit -m “Large file included”
```
