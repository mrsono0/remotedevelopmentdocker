FROM mrsono0/devremotecontainers:oraclejdk8

ARG BUILD_DATE
ENV TERM=xterm

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
	bash-completion \
	ca-certificates \
	ccache \
	devscripts \
	file \
	fonts-texgyre \
	g++ \
	gfortran \
	gsfonts \
	libblas-dev \
	libbz2-1.0 \
	libcurl4 \
	libicu63 \
	libjpeg62-turbo \
	libopenblas-dev \
	libpangocairo-1.0-0 \
	libpcre3 \
	libpng16-16 \
	libreadline7 \
	libtiff5 \
	liblzma5 \
	locales \
	make \
	unzip \
	zip \
	zlib1g \
	&& BUILDDEPS="curl \
	libbz2-dev \
	libcairo2-dev \
	libcurl4-openssl-dev \
	libpango1.0-dev \
	libjpeg-dev \
	libicu-dev \
	libpcre3-dev \
	libpng-dev \
	libreadline-dev \
	libtiff5-dev \
	liblzma-dev \
	libx11-dev \
	libxt-dev \
	perl \
	rsync \
	subversion tcl8.6-dev \
	tk8.6-dev \
	texinfo \
	texlive-extra-utils \
	texlive-fonts-recommended \
	texlive-fonts-extra \
	texlive-latex-recommended \
	x11proto-core-dev \
	xauth \
	xfonts-base \
	xvfb \
	zlib1g-dev" \
	&& apt-get install -y --no-install-recommends $BUILDDEPS \
	&& cd tmp/ \
	## Download source code
	&& svn co https://svn.r-project.org/R/trunk R-devel \
	## Extract source code
	&& cd R-devel \
	## Get source code of recommended packages
	&& ./tools/rsync-recommended \
	## Set compiler flags
	&& R_PAPERSIZE=letter \
	R_BATCHSAVE="--no-save --no-restore" \
	R_BROWSER=xdg-open \
	PAGER=/usr/bin/pager \
	PERL=/usr/bin/perl \
	R_UNZIPCMD=/usr/bin/unzip \
	R_ZIPCMD=/usr/bin/zip \
	R_PRINTCMD=/usr/bin/lpr \
	LIBnn=lib \
	AWK=/usr/bin/awk \
	CFLAGS="-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g" \
	CXXFLAGS="-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g" \
	## Configure options
	./configure --enable-R-shlib \
	--enable-memory-profiling \
	--with-readline \
	--with-blas \
	--with-tcltk \
	--disable-nls \
	--with-recommended-packages \
	## Build and install
	&& make \
	&& make install \
	## Add a default CRAN mirror
	&& echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl')" >> /usr/local/lib/R/etc/Rprofile.site \
	## Add a library directory (for user-installed packages)
	&& mkdir -p /usr/local/lib/R/site-library \
	&& chown root:staff /usr/local/lib/R/site-library \
	&& chmod g+wx /usr/local/lib/R/site-library \
	## Fix library path
	&& echo "R_LIBS_USER='/usr/local/lib/R/site-library'" >> /usr/local/lib/R/etc/Renviron \
	&& echo "R_LIBS=\${R_LIBS-'/usr/local/lib/R/site-library:/usr/local/lib/R/library:/usr/lib/R/library'}" >> /usr/local/lib/R/etc/Renviron \
	## install packages from date-locked MRAN snapshot of CRAN
	&& [ -z "$BUILD_DATE" ] && BUILD_DATE=$(TZ="America/Los_Angeles" date -I) || true \
	&& MRAN=https://mran.microsoft.com/snapshot/${BUILD_DATE} \
	&& echo MRAN=$MRAN >> /etc/environment \
	&& export MRAN=$MRAN \
	## MRAN becomes default only in versioned images
	## Use littler installation scripts
	# && Rscript -e "install.packages(c('littler', 'docopt'), repo = '$MRAN')" \
	&& ln -s /usr/local/lib/R/site-library/littler/examples/install2.r /usr/local/bin/install2.r \
	&& ln -s /usr/local/lib/R/site-library/littler/examples/installGithub.r /usr/local/bin/installGithub.r \
	&& ln -s /usr/local/lib/R/site-library/littler/bin/r /usr/local/bin/r \
	## TEMPORARY WORKAROUND to get more robust error handling for install2.r prior to littler update
	&& curl -O /usr/local/bin/install2.r https://github.com/eddelbuettel/littler/raw/master/inst/examples/install2.r \
	# && chmod +x /usr/local/bin/install2.r \
	## Clean up from R source install
	&& cd / \
	&& rm -rf /tmp/* \
	&& apt-get remove --purge -y $BUILDDEPS \
	&& apt-get autoremove -y \
	&& apt-get autoclean -y \
	&& rm -rf /var/lib/apt/lists/*

# Install R modules
RUN R -e "install.packages('rjson')" && \
	R -e "install.packages('XML')" && \
	R -e "install.packages('xml2')" && \
	R -e "install.packages('charlatan')" && \
	R -e "install.packages('httpuv')" && \
	R -e "install.packages('curl')" && \
	R -e "install.packages('httr')" && \
	R -e "install.packages('shiny')" && \
	R -e "install.packages('rmarkdown')" && \
	R -e "install.packages('knitr')" && \
	R -e "install.packages('caTools')" && \
	R -e "install.packages('writexl')" && \
	R -e "install.packages('rlist')" && \
	R -e "install.packages('tictoc')"&& \
	R -e "install.packages('kableExtra')"

RUN echo "install.packages(c('repr', 'IRdisplay', 'RSentiment', 'IRkernel', 'igraph'), repos='https://mirror.las.iastate.edu/CRAN')" | R --vanilla
RUN echo "IRkernel::installspec(user = FALSE)" | R --vanilla
# RUN echo "setRepositories(ind=c(2)); source('https://bioconductor.org/biocLite.R'); biocLite('gRain',suppressUpdates=TRUE,ask=FALSE);biocLite('Rgraphviz',suppressUpdates=TRUE,ask=FALSE);" | R --vanilla

RUN wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg \
	&& mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/ \
	&& wget -q https://packages.microsoft.com/config/debian/10/prod.list \
	&& mv prod.list /etc/apt/sources.list.d/microsoft-prod.list \
	&& chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg \
	&& chown root:root /etc/apt/sources.list.d/microsoft-prod.list

RUN apt-get update \
	&& apt-get install apt-transport-https
RUN apt-get update \
	&& apt-get -y install dotnet-sdk-3.0 dotnet-runtime-3.0

# Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

COPY ETC/Mikhail-Arkhipov.r-0.0.6.vsix \
	 ETC/Ikuyadeu.r-1.1.8.vsix \
	 /home/vscode/
RUN code-server --install-extension /home/vscode/Mikhail-Arkhipov.r-0.0.6.vsix \
	&& code-server --install-extension /home/vscode/Ikuyadeu.r-1.1.8.vsix
USER vscode
RUN code-server --install-extension /home/vscode/Mikhail-Arkhipov.r-0.0.6.vsix \
	&& code-server --install-extension /home/vscode/Ikuyadeu.r-1.1.8.vsix
USER root
RUN rm -f /home/vscode/*.vsix