FROM mrsono0/devremotecontainers:python3.7
# FROM mrsono0/devremotecontainers:miniconda3 as oraclejdk11

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
	curl zip gzip unzip wget libzmq3-dev 

ENV JAVA_FILE jdk-11.0.5_linux-x64_bin.tar.gz
COPY ETC/${JAVA_FILE} /usr/local/
RUN cd /usr/local; tar -zxvf ${JAVA_FILE}; rm /usr/local/${JAVA_FILE}; ls -al; cd /

ENV JAVA_HOME /usr/local/jdk-11.0.5
ENV PATH $JAVA_HOME/bin:$PATH
# RUN update-alternatives --config java

# basic smoke test
RUN	javac -version; \
	java -version

## https://github.com/SpencerPark/IJava
RUN wget https://github.com/SpencerPark/IJava/releases/download/v1.3.0/ijava-1.3.0.zip
RUN mv ijava-1.3.0.zip /usr/local/; cd /usr/local; unzip ijava-1.3.0.zip; python /usr/local/install.py --sys-prefix; rm -r java install.py ijava-1.3.0.zip

RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
    #
    # Install git, process tools, lsb-release (common in install instructions for CLIs)
    && apt-get -y install procps lsb-release \
    #
    # Allow for a consistant java home location for settings - image is changing over time
    && if [ ! -d "/docker-java-home" ]; then ln -s "${JAVA_HOME}" /docker-java-home; fi \
    && if [ ! -d "/usr/local/default-jdk" ]; then ln -s "${JAVA_HOME}" /usr/local/default-jdk; fi 

RUN unset JAVA_VERSION JAVA_BASE_URL JAVA_URL_VERSION

COPY ETC/vscjava.vscode-java-pack-0.8.1.vsix \
  ETC/redhat.java-0.53.1.vsix \
  ETC/vscjava.vscode-java-debug-0.23.0.vsix \
  ETC/vscjava.vscode-java-dependency-0.6.0.vsix \
  ETC/vscjava.vscode-java-test-0.21.0.vsix \
  ETC/vscjava.vscode-maven-0.20.0.vsix \
  ETC/adashen.vscode-tomcat-0.11.1.vsix \
  ETC/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  ETC/Pivotal.vscode-spring-boot-1.13.0.vsix \
  ETC/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
  ETC/vscjava.vscode-spring-initializr-0.4.6.vsix \
  /home/vscode/

RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
  && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
  && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
  && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER vscode
RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
  && code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \
  && code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \
  && code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
  && code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
  && code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
USER root
RUN rm -f /home/vscode/*.vsix

# # make some useful symlinks that are expected to exist
# RUN cd /usr/local/bin \
# 	&& ln -s idle3 idle \
# 	&& ln -s pydoc3 pydoc \
# 	&& ln -s python3 python \
# 	&& ln -s python3-config python-config

# if this is called "PIP_VERSION", pip explodes with "ValueError: invalid truth value '<VERSION>'"
ENV PYTHON_PIP_VERSION 19.3.1
# https://github.com/pypa/get-pip
ENV PYTHON_GET_PIP_URL https://github.com/pypa/get-pip/raw/ffe826207a010164265d9cc807978e3604d18ca0/get-pip.py
ENV PYTHON_GET_PIP_SHA256 b86f36cc4345ae87bfd4f10ef6b2dbfa7a872fbff70608a1e43944d283fd0eee

RUN set -ex; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	apt-get update; \
	apt-get install -y --no-install-recommends wget; \
	\
	wget -O get-pip.py "$PYTHON_GET_PIP_URL"; \
	echo "$PYTHON_GET_PIP_SHA256 *get-pip.py" | sha256sum --check --strict -; \
	\
	apt-mark auto '.*' > /dev/null; \
	[ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	rm -rf /var/lib/apt/lists/*; \
	\
	python get-pip.py \
	--disable-pip-version-check \
	--no-cache-dir \
	"pip==$PYTHON_PIP_VERSION" \
	; \
	pip --version; \
	\
	find /usr/local -depth \
	\( \
	\( -type d -a \( -name test -o -name tests -o -name idle_test \) \) \
	-o \
	\( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
	\) -exec rm -rf '{}' +; \
	rm -f get-pip.py

RUN bash -c "pip install --no-input pylint"

ENV RUSTUP_HOME=/usr/local/rustup 
ENV CARGO_HOME=/usr/local/cargo 
ENV PATH=/usr/local/jdk-11.0.5/bin:/usr/local/cargo/bin:/usr/local/rustup:$PATH 
ENV RUST_VERSION=1.40.0 

RUN set -eux; \
    dpkgArch="$(dpkg --print-architecture)"; \
    case "${dpkgArch##*-}" in \
        amd64) rustArch='x86_64-unknown-linux-gnu'; rustupSha256='e68f193542c68ce83c449809d2cad262cc2bbb99640eb47c58fc1dc58cc30add' ;; \
        armhf) rustArch='armv7-unknown-linux-gnueabihf'; rustupSha256='7c1c329a676e50c287d8183b88f30cd6afd0be140826a9fbbc0e3d717fab34d7' ;; \
        arm64) rustArch='aarch64-unknown-linux-gnu'; rustupSha256='d861cc86594776414de001b96964be645c4bfa27024052704f0976dc3aed1b18' ;; \
        i386) rustArch='i686-unknown-linux-gnu'; rustupSha256='89f1f797dca2e5c1d75790c3c6b7be0ee473a7f4eca9663e623a41272a358da0' ;; \
        *) echo >&2 "unsupported architecture: ${dpkgArch}"; exit 1 ;; \
    esac; \
    url="https://static.rust-lang.org/rustup/archive/1.20.2/${rustArch}/rustup-init"; \
    wget "$url"; \
    echo "${rustupSha256} *rustup-init" | sha256sum -c -; \
    chmod +x rustup-init; \
    ./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION; \
    rm rustup-init; \
    chmod -R a+w $RUSTUP_HOME $CARGO_HOME; \
    rustup --version; \
    cargo --version; \
    rustc --version; \
    cargo install evcxr_jupyter --no-default-features; \
	chmod -R 777 /usr/local/cargo/registry;

RUN unset RUST_VERSION MAVEN_CONFIG && \
    echo "export PATH=$PATH" >> /home/vscode/.bashrc && \
	echo "export USER=vscode" >> /home/vscode/.bashrc && \
    echo "export PATH=$PATH" >> /root/.bashrc && \
	echo "export USER=root" >> /root/.bashrc && \
	rustup update && \
	rustup component add rls rust-analysis rust-src 

COPY ETC/rust-lang.rust-0.7.0.vsix \
    ETC/ms-python.python-2020.1.57204.vsix \
    /home/vscode/
RUN code-server --install-extension /home/vscode/rust-lang.rust-0.7.0.vsix
USER vscode
RUN code-server --install-extension /home/vscode/rust-lang.rust-0.7.0.vsix
USER root
RUN rm -f /home/vscode/*.vsix

RUN pip3 install --no-cache-dir --upgrade \
    numpy \
    pandas \
    pandas-datareader \
    pandas-td \
    ipywidgets \
    pypki2 \
    ipydeps \
    jupyterhub \
    bash_kernel \
    beakerx  \
    jupyter_dashboards \
    py4j \
    && jupyter notebook --generate-config

RUN evcxr_jupyter --install \
    && beakerx install \
    && python3 -m bash_kernel.install

# RUN pip3 install http://github.com/nbgallery/nbgallery-extensions/tarball/master#egg=jupyter_nbgallery && \
#     echo "### Install jupyter extensions" && \
#     jupyter serverextension enable --py jupyterlab && \
#     jupyter nbextension enable --py --sys-prefix widgetsnbextension && \
#     jupyter serverextension enable --py jupyter_nbgallery && \
#     jupyter nbextension install --py jupyter_nbgallery && \
#     jupyter nbextension enable jupyter_nbgallery --py && \
#     jupyter dashboards quick-setup --sys-prefix 


USER vscode
RUN evcxr_jupyter --install
USER root

# RUN rm -rf /opt/conda/share/jupyter/kernels/clojure && \
#     rm -rf /opt/conda/share/jupyter/kernels/groovy && \
#     rm -rf /opt/conda/share/jupyter/kernels/scala && \
#     rm -rf /opt/conda/share/jupyter/kernels/sql && \
#     rm -rf /opt/conda/share/jupyter/kernels/xcpp11 

    #
    # Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*