FROM mrsono0/devremotecontainers:hadoop3.1.2 

ENV SCALA_VERSION 2.13.1
ENV SCALA_TARBALL http://www.scala-lang.org/files/archive/scala-$SCALA_VERSION.deb

RUN apt-get update --fix-missing && \
    apt install -y default-jre-headless curl && \
    echo "===> install Scala"  && \
    curl -sSL $SCALA_TARBALL -o scala.deb             && \
    dpkg -i scala.deb                                 && \
    echo "===> clean up..."  && \
    rm -f *.deb  
    # apt-get remove -y --auto-remove curl   && \
    # apt-get clean                          && \
    # rm -rf /var/lib/apt/lists/*

# ENV ENABLE_INIT_DAEMON true
# ENV INIT_DAEMON_BASE_URI http://identifier/init-daemon
# ENV INIT_DAEMON_STEP spark_master_init

ENV SPARK_VERSION=2.4.4
ENV HADOOP_VERSION=2.7
ENV SPARK_URL=https://archive.apache.org/dist/spark/spark-${SPARK_VERSION}/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz
ENV SPARK_HOME=/opt/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}
ENV PATH=${SPARK_HOME}/bin:${SPARK_HOME}/sbin:$PATH

# COPY wait-for-step.sh /
# COPY execute-step.sh /
# COPY finish-step.sh /

RUN curl -fSL "$SPARK_URL" -o /tmp/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz \
    && tar -xvzf /tmp/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz -C /opt/ \
    && rm /tmp/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz \
    && ln -s /opt/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION} /opt/spark \
    && pip install pyspark hide_code plotly pymysql boto statsmodels sympy 

ADD ETC/spark-defaults.conf $SPARK_HOME/conf/spark-defaults.conf
ADD ETC/pyspark_kernel.json /usr/local/share/jupyter/kernels/pyspark/kernel.json
RUN jupyter nbextension install --py hide_code && \
    jupyter nbextension enable --py hide_code && \
    jupyter serverextension enable --py hide_code

# toree kernel
RUN pip install toree
RUN jupyter toree install \
    --spark_home=$SPARK_HOME \
    --spark_opts='--master=local[0]'
#     --kernel_name=PySpark \
#     --interpreters=PySpark

#Give permission to execute scripts
# RUN chmod +x /wait-for-step.sh && chmod +x /execute-step.sh && chmod +x /finish-step.sh

# Fix the value of PYTHONHASHSEED
# Note: this is needed when you use Python 3.3 or greater
ENV PYTHONHASHSEED 1

COPY ETC/entrypoint.hadoop.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh && \
    echo "export PATH=$PATH" >> /home/vscode/.bashrc && \
    echo "export PATH=$PATH" >> /root/.bashrc 
